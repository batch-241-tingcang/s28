// insertOne

db.rooms.insertOne({
	name: "single",
	accomodates: 2.0,
	price: 1000.0,
	description: "A single room with all the basic necessities",
	rooms_available: 10.0,
	isAvailable: false
})

// insertMany

db.rooms.insertMany([
{
	name: "double",
	accomodates: 3.0,
	price: 2000.0,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5.0,
	isAvailable: false
},
{
	name: "queen",
	accomodates: 4.0,
	price: 4000.0,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15.0,
	isAvailable: false
}

]);

//  find

db.rooms.find({name: 'double'})

// updateOne

db.rooms.updateOne(
	{name: "queen"},
	{
		$set: {
			name: "queen",
			accomodates: 4.0,
			price: 4000.0,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_available: 0,
			isAvailable: false
		}
	}

	);

db.rooms.find({rooms_available: 0});

// deletemany

db.rooms.deleteMany({
	rooms_available: 0
})

db.rooms.find();


db.getCollection('rooms').find({})


















